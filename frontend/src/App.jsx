import * as Pages from "./pages/index";

function App() {
  return (
    <div className="p-4 h-screen flex items-center justify-center">
      {/* <Pages.SignUp /> */}
      {/* <Pages.Login /> */}
      <Pages.Home />
    </div>
  );
}

export default App;
